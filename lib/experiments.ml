(** Deals with the statefull part of the program *)

open Containers

let compare = CCOrd.pair CCOrd.int CCOrd.string

let load_logs file =
  if not (Sys.file_exists file) then []
  else
    CCIO.(with_in file read_lines_l)
    |> List.map State_j.log_of_string
    |> List.sort compare

let load_experiments iterations successful_job instances_dir suffix =
  CCIO.File.walk_seq instances_dir
  |> Seq.filter_map (function
       | `Dir, _ -> None
       | `File, file ->
           if
             (not (CCIO.File.is_directory file))
             && Filename.check_suffix file suffix
           then Some file
           else None)
  |> Seq.to_list
  |> List.product (fun a b -> (a, b)) List.(1 -- iterations)
  |> List.filter (fun (i, file) ->
         not @@ List.mem (i, Unix.realpath file) successful_job)

open Lwt.Syntax
open Lwt.Infix

let make_cmd cpu name_queue template file : Lwt_process.command =
  ( "",
    Array.concat
      [
        [| "cgexec"; "-g"; Cgroup.controllers ^ ":" ^ name_queue |];
        [| "taskset"; "--cpu-list"; Int.to_string cpu |];
        [| "setarch"; "x86_64"; "--addr-no-randomize" |];
        [| "time" |];
        Array.map
          (fun arg -> if String.equal "{}" arg then file else arg)
          template;
      ] )

let rec rec_mkdir path file_perm =
  if String.(path = ".") then Lwt.return_unit
  else
    let* exist = Lwt_unix.file_exists path in
    if exist then Lwt.return_unit
    else
      let* () = rec_mkdir (Filename.dirname path) file_perm in
      Lwt.catch
        (fun () -> Lwt_unix.mkdir path file_perm)
        (function
          | Unix.Unix_error (Unix.EEXIST, _, _) -> Lwt.return_unit
          | e -> raise e)

let append_write_flags =
  [
    Lwt_unix.O_WRONLY; Lwt_unix.O_NONBLOCK; Lwt_unix.O_APPEND; Lwt_unix.O_CREAT;
  ]

let runner log result_dir timeout cmd_template experiments_stream
    (cpu, name_queue) =
  let rec aux () =
    let* next = Lwt_stream.get experiments_stream in
    match next with
    | None -> Lwt.return_unit
    | Some (i, input_file) ->
        (let* exist = Lwt_unix.file_exists input_file in
         if not exist then
           Logs_lwt.warn (fun m -> m "%s no longer exists" input_file)
         else
           (* Prepare running the experiment *)
           let cmd = make_cmd cpu name_queue cmd_template input_file in
           let name =
             Filename.remove_extension (Filename.basename input_file)
           in
           let directory =
             Filename.concat
               (Filename.concat result_dir (Filename.dirname input_file))
               (Int.to_string i)
           in
           let out_filename = Filename.concat directory (name ^ ".out") in
           let err_filename = Filename.concat directory (name ^ ".err") in
           let time_filename = Filename.concat directory (name ^ ".time") in
           let* () =
             Logs_lwt.debug (fun m ->
                 m "Creation of output directory: %s" directory)
           in
           let* () = rec_mkdir directory 0o750 in
           let* fd_out =
             Lwt_unix.openfile out_filename append_write_flags 0o640
           in
           let* fd_err =
             Lwt_unix.openfile err_filename append_write_flags 0o640
           in
           let time = ref 0. in
           let counter = Mtime_clock.counter () in
           let status =
             (Lwt_process.open_process_none ?timeout ~stdin:`Close
                ~stdout:(`FD_move (Lwt_unix.unix_file_descr fd_out))
                ~stderr:(`FD_move (Lwt_unix.unix_file_descr fd_err))
                cmd)
               #status
           in
           Lwt.on_success status (fun _ ->
               time := Mtime.Span.to_float_ns (Mtime_clock.count counter));
           Lwt.on_cancel status (fun () -> Cgroup.kill name_queue);
           status >>= function
           | Unix.WEXITED 0 ->
               Logs_lwt.info (fun m ->
                   m "Computation was successful on %s" input_file)
               >>= fun () ->
               Lwt_io.write_line log
                 (State_j.string_of_log (i, Unix.realpath input_file))
               >>= fun () ->
               Lwt_io.with_file ~flags:append_write_flags ~mode:Lwt_io.output
                 time_filename (fun tc ->
                   Lwt_io.write_line tc (Float.to_string !time))
           | _ ->
               Cgroup.kill name_queue;
               Logs_lwt.err (fun m -> m "Computation failed on %s" input_file))
        >>= aux
  in
  aux ()
