open Containers

let main = "ares_cgroup"
let controllers = "memory"
let get_sub_group i = Printf.sprintf "%s/experiment_%i" main i
let control = main ^ "/ares_control"

(** Create a new sub cgroup *)
let create name =
  let login = Unix.getlogin () in
  Utils.exec_shell_cmd "cgcreate"
    [ "-a"; login; "-t"; login; "-g"; controllers ^ ":" ^ name ]

(** Set the memory limit of a cgroup *)
let set_memory_limit name limit =
  CCIO.with_out
    (Filename.concat (Filename.concat "/sys/fs/cgroup" name) "memory.high")
    (fun oc -> output_string oc limit)
(* TODO use cgset *)

(** Kill all process in a cgroup *)
let kill name =
  CCIO.with_out
    (Filename.concat (Filename.concat "/sys/fs/cgroup" name) "cgroup.kill")
    (fun oc -> output_char oc '1')
(* TODO use cgset *)

(** Initialise a cgroup hierarchy for ares to use to control the benchmarked process*)
let init () =
  let open Result.Infix in
  let login = Unix.getlogin () in
  Utils.exec_shell_cmd "sudo"
    [
      "--"; "cgcreate"; "-a"; login; "-t"; login; "-g"; controllers ^ ":" ^ main;
    ]
  >>= fun () ->
  create control >>= fun () ->
  Utils.exec_shell_cmd "sudo"
    [
      "--";
      "/bin/sh";
      "-c";
      "echo "
      ^ Int.to_string (Unix.getpid ())
      ^ " >> /sys/fs/cgroup/" ^ control ^ "/cgroup.procs";
    ]

(* Maybe use cpuset.cpus to control which cpu is used *)
