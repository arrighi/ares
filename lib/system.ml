open Containers
open CCResult.Infix

(* https://github.com/ocaml-bench/ocaml_bench_scripts/*)

type feature_state = On | Off

let is_feature_enable file =
  try
    match CCIO.with_in file input_char with
    | '0' -> Ok false
    | '1' -> Ok true
    | _ -> Error (Printf.sprintf "I don't understant the content of %s" file)
  with e -> Error (Printexc.to_string e)

let is_ht_on () = is_feature_enable "/sys/devices/system/cpu/smt/active"

let switch_off_ht () =
  Utils.exec_shell_cmd "sudo"
    [ "--"; "/bin/sh"; "-c"; "echo off >> /sys/devices/system/cpu/smt/control" ]

let is_turbo_on () = is_feature_enable "/sys/devices/system/cpu/cpufreq/boost"

let switch_off_turbo () =
  Utils.exec_shell_cmd "sudo"
    [ "--"; "/bin/sh"; "-c"; "echo 0 >> /sys/devices/system/cpu/cpufreq/boost" ]

let parse_cpu cpu_list =
  String.split_on_char ',' cpu_list
  |> List.map (fun cpu ->
         match String.split_on_char '-' cpu with
         | [ n ] -> Ok [ int_of_string n ]
         | [ start; stop ] ->
             Ok List.(int_of_string start -- int_of_string stop)
         | _ -> Error ("Unknown cpu value: " ^ cpu))
  |> Result.flatten_l >|= List.flatten

(** Get the list of isolated cpu. *)
let get_isolated_cpu () =
  try
    match CCIO.with_in "/sys/devices/system/cpu/isolated" CCIO.read_line with
    | None | Some "" -> Error "No isolated CPU"
    | Some s -> parse_cpu s
  with e -> Error ("get_isolated_cpu: " ^ Printexc.to_string e)

(** Get the list of isolated cpu. *)
let get_online_cpu () =
  try
    CCIO.with_in "/sys/devices/system/cpu/online" CCIO.read_line
    |> CCOption.get_exn_or "Nothing to read"
    |> parse_cpu
  with e -> Error ("get_online_cpu: " ^ Printexc.to_string e)

(** Available cpu for the benchmark: intersection of isolated and online cpu *)
let get_available_cpu () =
  let* isolated_cpu = get_isolated_cpu () in
  let+ online_cpu = get_online_cpu () in
  List.inter ~eq:Int.equal isolated_cpu online_cpu

let get_mem () =
  let open CCOption.Infix in
  (let* l = CCIO.with_in "/proc/meminfo" CCIO.read_line in
   let* memory = List.get_at_idx 1 (String.split_on_char ':' l) in
   match String.split_on_char ' ' (String.trim memory) with
   | [ value; exp ] -> Some (int_of_string value, String.sub exp 0 1)
   | _ -> None)
  |> Result.of_opt

(* To control a process use cgroups. cgroup.kill can kill all the process, memory can be limited for a fair share, cpu alocation maybe or with tasks *)

(** Check the if ht and turbo is disable and return the list of available CPU *)
let setup () =
  let* ht = is_ht_on () in
  (if ht then switch_off_ht () else Ok ()) >>= fun () ->
  let* turbo = is_turbo_on () in
  (if turbo then switch_off_turbo () else Ok ()) >>= fun () ->
  let* available_cpu = get_available_cpu () in
  let* experiments_queues =
    match available_cpu with
    | [] -> Error "No CPU available"
    | l -> Ok (List.map (fun cpu -> (cpu, Cgroup.get_sub_group cpu)) l)
  in
  let* memory, exp = get_mem () in
  let* () = Cgroup.init () in
  List.map (fun (_, sub_group) -> Cgroup.create sub_group) experiments_queues
  |> Result.flatten_l
  >>= fun _ ->
  let memory_per_node =
    Printf.sprintf "%i%s" (memory / (List.length experiments_queues + 1)) exp
  in
  List.iter
    (fun (_, sub_group) -> Cgroup.set_memory_limit sub_group memory_per_node)
    experiments_queues;
  Result.return experiments_queues

(* TODO: Setting default pstate to performance *)
