open Containers

let exec_shell_cmd cmd arg =
  Format.printf "@[%s %a@]@." cmd
    (List.pp ~pp_sep:(fun fmt () -> Format.fprintf fmt "@ ") Format.string)
    arg;
  match Unix.(system (Filename.quote_command cmd arg)) with
  | Unix.WEXITED 0 -> Ok ()
  | Unix.WEXITED n -> Error (Printf.sprintf "Cmd return %i" n)
  | Unix.WSIGNALED n -> Error (Printf.sprintf "Cmd was killed by %i" n)
  | Unix.WSTOPPED n -> Error (Printf.sprintf "Cmd was stopped by %i" n)
