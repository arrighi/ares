(* TODO list
 * handle signal to stop properly
 *)

open Containers
open Lwt.Syntax
open Ares

let append_write_flags =
  [ Unix.O_WRONLY; Unix.O_NONBLOCK; Unix.O_APPEND; Unix.O_CREAT ]

let main stopped instances_dir iterations suffix result_dir logs_file timeout
    cmd_template () =
  let cmd_template = Array.of_list (String.split_on_char ' ' cmd_template) in
  let successful_job = Experiments.load_logs logs_file in
  Logs.info (fun m -> m "Log file loaded");
  Logs.info (fun m -> m "Start listing input files.");
  let experiments =
    Experiments.load_experiments iterations successful_job instances_dir suffix
  in
  Logs.info (fun m -> m "Experiments loaded: %i" (List.length experiments));
  let experiments_stream = Lwt_stream.of_list experiments in

  match System.setup () with
  | Error er -> Logs_lwt.err (fun m -> m "%s" er)
  | Ok experiments_setup ->
      Lwt.pick
        [
          (let* () = stopped in
           Logs_lwt.info (fun m -> m "Computation stopped"));
          Lwt_io.with_file ~flags:append_write_flags ~mode:Lwt_io.output
            logs_file (fun log ->
              List.map
                (Experiments.runner log result_dir timeout cmd_template
                   experiments_stream)
                experiments_setup
              |> Lwt.join);
        ]

(* Logs *)
let lwt_reporter () =
  let buf_fmt ~like =
    let b = Buffer.create 512 in
    ( Fmt.with_buffer ~like b,
      fun () ->
        let m = Buffer.contents b in
        Buffer.reset b;
        m )
  in
  let app, app_flush = buf_fmt ~like:Fmt.stdout in
  let dst, dst_flush = buf_fmt ~like:Fmt.stderr in
  let reporter = Logs_fmt.reporter ~app ~dst () in
  let report src level ~over k msgf =
    let k () =
      let write () =
        match level with
        | Logs.App -> Lwt_io.write Lwt_io.stdout (app_flush ())
        | _ -> Lwt_io.write Lwt_io.stderr (dst_flush ())
      in
      let unblock () =
        over ();
        Lwt.return_unit
      in
      Lwt.finalize write unblock |> Lwt.ignore_result;
      k ()
    in
    reporter.Logs.report src level ~over:(fun () -> ()) k msgf
  in
  { Logs.report }

(* Cmdliner *)
open Cmdliner

let setup_log style_renderer level =
  Fmt_tty.setup_std_outputs ?style_renderer ();
  Logs.set_level level;
  Logs.set_reporter (lwt_reporter ());
  ()

let setup_log =
  Term.(const setup_log $ Fmt_cli.style_renderer () $ Logs_cli.level ())

let cmdline stopped =
  let timeout =
    let doc = "Time limit" in
    Arg.(value & opt (some float) None & info [ "t"; "time" ] ~doc)
  in
  let iterations =
    let doc = "Number of iterations" in
    Arg.(value & opt int 1 & info [ "n"; "iter" ] ~doc)
  in
  let instances_dir =
    let doc = "Directory of the instances" in
    Arg.(
      required
      & opt (some dir) None
      & info [ "i"; "instances" ] ~docv:"INST" ~doc)
  in
  let suffix =
    let doc = "Suffix of the input files" in
    Arg.(value & opt string "" & info [ "s"; "suffix" ] ~doc)
  in
  let results_dir =
    let doc = "Directory of the results" in
    Arg.(
      required
      & opt (some dir) None
      & info [ "r"; "results" ] ~docv:"RESULT" ~doc)
  in
  let log =
    let doc = "Name of the log file" in
    Arg.(value & opt string "ares.log" & info [ "l"; "log" ] ~doc)
  in
  let cmd_template =
    let doc =
      "Command to be run, use {} as a place holder for the name of the file"
    in
    Arg.(value & pos 0 string "" & info [] ~docv:"CMD" ~doc)
  in
  let doc = "Run experiments" in
  let exits = Cmd.Exit.defaults in
  let man = [ `S Manpage.s_description; `P "Run experiments" ] in
  Cmd.v
    (Cmd.info "ares" ~doc ~sdocs:Manpage.s_common_options ~exits ~man)
    Term.(
      const Lwt_main.run
      $ (const main $ const stopped $ instances_dir $ iterations $ suffix
       $ results_dir $ log $ timeout $ cmd_template $ setup_log))

let () =
  let stopped, resolver = Lwt.task () in
  (Lwt.async_exception_hook :=
     function
     | Lwt_io.Channel_closed _ -> ()
     | e -> Logs.warn (fun m -> m "Exception raised: %s" (Printexc.to_string e)));
  Sys.set_signal Sys.sigint
    (Sys.Signal_handle (fun _ -> Lwt.wakeup resolver ()));
  Sys.set_signal Sys.sigterm
    (Sys.Signal_handle (fun _ -> Lwt.wakeup resolver ()));
  exit @@ Cmd.eval (cmdline stopped)
